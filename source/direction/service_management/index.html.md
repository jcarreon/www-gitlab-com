---
layout: markdown_page
title: "Product Direction - Service Management"
description: "Connect users and customers of your product to support"
canonical_path: "/direction/service_management/"
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Service Management

| | |
| --- | --- |
| Stage | [Service Management](/direction/service_management/) |
| Content Last Reviewed | `2023-07-31` |

### Overview

ITIL Definition: A set of specialised organisational capabilities for enabling value for customers in the form of services.
